/**
 * 冲突测试2
 *
 * @author liudo
 * @version 1.0
 * @project git-demo
 * @date 2023/9/6 15:16:05
 */
public class ConflictTest2 {

    public static void main(String[] args) {
        int added = add(1, 2);
        System.out.println("added = " + added);
    }

    /**
     * 添加
     *
     * @param a 一
     * @param b b
     * @return int
     */
    public static int add(int a, int b) {
        // todo 这里有一个Bug需要修复
        return a - b;
    }
}
